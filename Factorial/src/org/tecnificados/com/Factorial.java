package org.tecnificados.com;

public class Factorial {

	public int num;

	public Factorial() {
		super();
		this.num = 1;
	}

	public Factorial(int num) throws Exception {
		super();
		testNum(num);
		this.num = num;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) throws Exception {
		testNum(num);
		this.num = num;
	}
	
	public int calc() throws Exception
	{	
		try
		{
			return operation(num);
		}
		catch (StackOverflowError e)
		{
			throw new Exception ("The num is to big for me :(");
		}
	}
	
	private int operation(int n)
	{	
		if ((n==1)||n==0){
			return 1;
		}
		else{
			return (n*operation(n-1));
		}
			
	}
	
	private void testNum(int n) throws Exception
	{
		if (n<0)
			throw new Exception("The factorial number must be greather or equal than 0");
	}

	public static void main(String[] args) {
		Factorial f;
		try {
			f = new Factorial(10);
			System.out.println("Factorial of: "+f.getNum());
			System.out.println("Result: "+f.calc());
		} catch (Exception e) {
			System.out.println("There was an error: "+e.getMessage());
			e.printStackTrace();
		}
		

	}

}
