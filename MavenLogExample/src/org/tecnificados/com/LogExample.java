package org.tecnificados.com;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LogExample {
	
	private static final Logger log = Logger.getLogger(LogExample.class);
	
	public static void main(String[] args) {
		
		try {                            
		     PropertyConfigurator.configure("log4j.properties");
		     log.info("Sistema de log iniciado");                           

		} catch (Exception e) {
		     e.printStackTrace();		     
		}
		
	}

}
